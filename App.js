import React, { Component } from 'react';
import { StyleSheet, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import MainStackRouter from './src/routers/MainStackRouter';
import configureStore from './src/store/configureStore';

const store = configureStore();

export default class App extends Component {

  componentDidMount() {
    StatusBar.setHidden(true);
  }

  render() {
    return (
      <Provider store={store} style={styles.container}>
        <MainStackRouter />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%'
  },
});