##### If problem third-party: 'config.h' file not found 
rm -rf node_modules/ && yarn cache clean && yarn install && rm -rf ~/.rncache
    then
cd node_modules/react-native/third-party/glog-0.3.4/ && ../../scripts/ios-configure-glog.sh

## Создание билда для android

##### First, from your app's root dir, run the following command:

react-native bundle --dev false --platform android --entry-file index.js --bundle-output ./android/app/build/intermediates/assets/debug/index.android.bundle --assets-dest ./android/app/build/intermediates/res/merged/debug

##### or 

mkdir -p android/app/src/main/assets && rm -rf android/app/build && react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res && cd android && ./gradlew assembleDebug

##### Then the second and final step:

cd android
./gradlew assembleDebug

##### If ERROR when run dev-mode, need clean all build android:

cd android
./gradlew clean

##### Generating the release APK:
cd android && ./gradlew assembleRelease

Run: react-native run-android --variant=release

## Создание билда для ios
react-native bundle --entry-file index.js --platform ios --dev false --bundle-output ios/main.jsbundle --assets-dest ios

react-native run-ios --configuration Release 