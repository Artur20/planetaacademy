import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  BackHandler,
  Platform
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import Orientation from 'react-native-orientation';
import RF from "react-native-responsive-fontsize"
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as MenuActions from '../../actions/menu'
import * as HomeActions from '../../actions/home'
import MenuBtn from '../../components/menuButton'

const unpressedClose = require('../../assets/img/close-btn.png');
const pressedClose = require('../../assets/img/btn_close_pressed.png');

class Menu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      bRatio: 1030 / 558,
      bWidth: null,
      bHeight: null,
      vw: (Platform.OS == 'ios' && Orientation.getInitialOrientation() === 'PORTRAIT') ? 2 :(Platform.OS == 'ios' && Orientation.getInitialOrientation() === 'LANDSCAPE') ? 2.5 : 3.5,
      showPressedClose: true,
      loaded: false,
    }
  }

  componentDidMount() {
    Orientation.lockToLandscape();

    setTimeout(()=> {
      if (Dimensions.get('window').height <= 558) {
        this.setState({
          bWidth: ((1030 * Dimensions.get('window').height) / 558) * 0.7,
          bHeight: Dimensions.get('window').height * 0.7,
        });
      } else {
        this.setState({
          bWidth: ((1030 * (Dimensions.get('window').width / this.state.bRatio)) / 558) * 0.7,
          bHeight: (Dimensions.get('window').width / this.state.bRatio) * 0.7,
        });
      }
      
      this.setState({ 
        loaded: true,
      });
    }, 100);

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.props.changePage('menu');
  }

  handleBackButtonClick = () => {
    this._back();
    return true;
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  _back = () => {
    const prevPage = this.props.navigation.getParam('prevPage', null);

    if (prevPage === 'home') {
      this.props.navigation.goBack();
    } else {
      Orientation.lockToPortrait();
      this.props.navigation.goBack();
    }
  }

  _navigateToWalkthrough = (root) => {
    const resetAction = StackActions.reset({
      index: 1,
      actions: [
        NavigationActions.navigate({ routeName: 'home' }),
        NavigationActions.navigate({ routeName: root })
      ]
    });
    this.props.navigation.dispatch(resetAction);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  renderButtonClose() {
    return (
      <Image
        style={ styles.closeButton }
        resizeMode='contain'
        source={ this.state.showPressedClose ? unpressedClose : pressedClose }
      />
    );
  }

  render() {
    return (
      <View>
        <ImageBackground 
          style={ styles.mainBackground } 
          resizeMode='cover' 
          source={require('../../assets/img/main-bg.png')}>
          {
            (this.state.loaded) ? (
              <ImageBackground 
                style={[styles.menuBackground, { width: this.state.bWidth, height: this.state.bHeight }]}
                source={require('../../assets/img/menu-bg.png')}>
                <Image
                  style={ styles.menuName }
                  resizeMode='contain' 
                  source={require('../../assets/img/menu-name.png')} 
                />
                <TouchableOpacity
                  activeOpacity={1}
                  style={styles.closeBtnBackground}
                  onPressIn={ () => this.setState({ showPressedClose: !this.state.showPressedClose }) }
                  onPressOut={ () => this._back() }>
                  {this.renderButtonClose()}
                </TouchableOpacity>
                <View style = {styles.menuContainer}>
                  <MenuBtn index={10} link={'home'} label={'Главная'} size={ RF(this.state.vw) } navigation={this.props.navigation} is_list_menu={false}/>
                  <MenuBtn index={11} link={'cleaneTeeth'} label={'Почистить зубы'} size={ RF(this.state.vw) } navigation={this.props.navigation} is_list_menu={false}/>
                  <MenuBtn index={12} link={'calendar'} label={'Дневник'} size={ RF(this.state.vw) } navigation={this.props.navigation} is_list_menu={false}/>
                  <MenuBtn index={12} link={'about'} label={'О центре'} size={ RF(this.state.vw) } navigation={this.props.navigation} is_list_menu={false}/>
                </View>
              </ImageBackground>        
            ) : false
          } 
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainBackground: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuBackground: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuName: {
    position: 'absolute',
    width: '40%',
    height: '22%',
    top: '-10%',
  },
  menuContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignItems: 'center',     
    width: '80%',
    paddingVertical: '8%'
  },  
  textMenuItem: {
    fontFamily: 'PeaceSans',
    color: 'white',
    textShadowColor: 'rgba(0, 0, 0, 1)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },  
  closeBtnBackground: {
    position: 'absolute',
    alignSelf: 'flex-end',
    width: '15%',
    height: '15%',
    top: '-5%',
    right: '-5%',
    zIndex: 20,
  },
  closeButton: {
    position: 'absolute',
    zIndex: 99,
    width: '100%',
    height: '100%',
  }
});

function mapStateToProps(state) {
  return {
    listMenu: state.menu.listMenu,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...MenuActions, ...HomeActions}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu)