import React, { Component } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Image,
  Dimensions,
  BackHandler,
  TouchableOpacity,
  Text,
  Linking,
  Modal
} from 'react-native';
import {Content, Icon, H2, H3} from 'native-base';
import resolveAssetSource from 'resolveAssetSource';
import Orientation from 'react-native-orientation';
import ImageViewer from 'react-native-image-zoom-viewer';
import HTMLView from 'react-native-htmlview';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {MAIN_COLOR} from '../../config'
import * as AboutActions from '../../actions/about';
import * as HomeActions from '../../actions/home'
import NavComponent from '../../components/navbar';


class About extends React.Component {
  static navigationOptions = NavComponent;
  constructor(props){
    super(props);
    this.state = {
      errors: [],
      gallery: [],
      openGallery: false,
      initialPage: 0,
      images: [
        require('../../assets/gallery/01.jpg'),
        require('../../assets/gallery/02.jpg'),
        require('../../assets/gallery/03.jpg'),
        require('../../assets/gallery/04.jpg'),
        require('../../assets/gallery/05.jpg'),
        require('../../assets/gallery/06.jpg'),
        require('../../assets/gallery/07.jpg'),
        require('../../assets/gallery/08.jpg'),
      ]
    }
    this._carousel = {};
    this._gallery = null;
    this.onChangeImage = this.onChangeImage.bind(this);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }
  
  componentDidMount() {
    Orientation.unlockAllOrientations();
    this.props.getAboutData();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.props.changePage('about');
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack();
    this.props.changeCounter(-1);
    return true;
  }

  openGallery = (index) => {
    let array = [];

    this.state.images.forEach((item) => {
      array.push({
        url: '',
        props: {
          source: item,
        }
      })
    });
    this.setState({gallery: array, openGallery: true, initialPage: index});
  }

  closeGallery = () => {
    this.setState({ openGallery: false });
  }

  onChangeImage (index) {
    this.setState({initialPage: index})
  }

  imagesSlider() {
    let {images} = this.state;

    return images.map((item, i) => {
      let imageObj = resolveAssetSource(item);
      return ( 
        <TouchableOpacity onPress={this.openGallery.bind(this, i)} key={i}>
          <Image source={item} style={{width: imageObj.width/4, height: 220, marginHorizontal: 3}} resizeMode='cover' />
        </TouchableOpacity>
      );
    });
  }

  render() {
    return ( 
      <ScrollView>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={true}
          showsVerticalScrollIndicator={false}
          style={{ marginHorizontal: -3}}
        >
        { this.imagesSlider() }
        </ScrollView>
        <Modal 
          visible={this.state.openGallery} 
          transparent={true} 
          style={{ flex: 1 }}
          onRequestClose={() => this.closeGallery.bind(this, true)}
          supportedOrientations={['portrait', 'landscape']}>
          <TouchableOpacity style={{ position: 'absolute', left: 20, top: 20, zIndex: 2 }} onPress={this.closeGallery.bind(this, true)}>
            <Icon style={{ color: '#fff' }} name='close' />
          </TouchableOpacity>
          <ImageViewer imageUrls={this.state.gallery} index={this.state.initialPage}/>
        </Modal>
        <Content style={{paddingVertical: 20, paddingHorizontal: 15}}>
          <H2 style={{marginBottom: 10}}>EG PLANETA ACADEMY</H2>
          <Text style={styles.simpleText}>Это международный образовательный проект в который входят образовательные организации и учреждения Франции, Испании, Германии, Великобритании, Китая, Мексики, Сербии, Италии. В проекте работают эксперты международного образования, преподаватели иностранных языков и психологи. Мы разные, но мы вместе! Нас объединяет идея интеллектуального развития людей и альтернативного образования без границ.</Text>
          <Text style={styles.simpleText}>Более подробную информацию вы можете найти на нашем сайте <Text style={{color: MAIN_COLOR}} onPress={()=> Linking.openURL('http://planetacademy.ru')}>http://planetacademy.ru</Text></Text>
          <H3 style={{marginVertical: 10}}>Адрес</H3>
          <Text style={{fontSize: 16, color: 'black', marginVertical: 3}}>Красногорск, Московская область, Россия, 141401</Text>
          <Text style={{fontSize: 16, color: 'black', marginVertical: 3}}>2-ой Митинский переулок дом 5</Text>
          <H3 style={{marginVertical: 10}}>Телефон</H3>
          <Text style={{fontSize: 16, color: 'black', marginVertical: 3}} onPress={()=> Linking.openURL('tel:+79859595131')}>+7 (985) 959-51-31</Text>
          <Text style={{fontSize: 16, color: 'black', marginVertical: 3}} onPress={()=> Linking.openURL('mailto:planetacademy@yandex.ru')}>planetacademy@yandex.ru</Text>
        </Content>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  aboutItemImage: {
    width: '100%',
    height: '100%'
  },
  carousel: {
    height: 230,
    backgroundColor: '#fff'
  },
  simpleText: {
    fontSize: 15,
    color: 'black',
    marginVertical: 5,
  }
});

function mapStateToProps(state) {
  return {
    text: state.about.about.text,
    home: state.home
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...AboutActions, ...HomeActions}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(About)