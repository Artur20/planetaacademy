import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  BackHandler,
  Platform
} from 'react-native';
import Orientation from 'react-native-orientation';
import RF from "react-native-responsive-fontsize"
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as MenuActions from '../../actions/menu'
import * as HomeActions from '../../actions/home'
import ItemCalendar from '../../components/calendar/itemCard'

const unpressed = require('../../assets/img/btn_menu-yellow.png');
const pressed = require('../../assets/img/btn_menu_pressed.png');
const unpressedClose = require('../../assets/img/close-btn.png');
const pressedClose = require('../../assets/img/btn_close_pressed.png');

class Calendar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      bWidth: null,
      bHeight: null,
      showPressed: true,
      showPressedClose: true
    }
  }

  componentDidMount() {
    if (Dimensions.get('window').height <= 558) {
      this.setState({
        bWidth: ((1030 * Dimensions.get('window').height) / 558) * 0.7,
        bHeight: Dimensions.get('window').height * 0.7,
      });
    } else {
      this.setState({
        bWidth: ((1030 * (Dimensions.get('window').width / this.state.bRatio)) / 558) * 0.7,
        bHeight: (Dimensions.get('window').width / this.state.bRatio) * 0.7,
      });
    }

    Orientation.lockToLandscape();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);    
    this.props.syncDataCalendar();
    this.props.changePage('calendar');
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this._back();
    return true;
  }

  _back = () => {
    this.props.navigation.goBack();
  }

  renderButton() {
    return (
      <Image
        style={ styles.btnMenu }
        resizeMode='contain'
        source={ this.state.showPressed ? unpressed : pressed }
      />
    );
  }

  renderButtonClose() {
    return (
      <Image
        style={ styles.closeButton }
        resizeMode='contain'
        source={ this.state.showPressedClose ? unpressedClose : pressedClose }
      />
    );
  }

  render() {
    return (
      <ImageBackground 
        style={ styles.mainBackground } 
        resizeMode='cover' 
        source={require('../../assets/img/main-bg.png')}>
        <ImageBackground 
          style={[styles.menuBackground, { width: this.state.bWidth, height: this.state.bHeight }]} 
          source={require('../../assets/img/menu-bg.png')}>
          <Image
            style={ styles.menuName }
            resizeMode='contain' 
            source={require('../../assets/img/calendar_name.png')} 
          />
          <TouchableOpacity
            activeOpacity={1}
            style={styles.closeBtnBackground}
            onPressIn={ () => this.setState({ showPressedClose: !this.state.showPressedClose }) }
            onPressOut={()=> {
              this._back();
            }}>
            {this.renderButtonClose()}
          </TouchableOpacity>
          <View style = {styles.menuContainer}>
            {
              (this.props.dataCalendar) ? (
                this.props.dataCalendar.map((item, index) => (
                  <ItemCalendar key={index} label={item.name} status={item.status} />
                ))
              ) : false
            }  
          </View>
        </ImageBackground>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.btnMenuImage}
          onPressIn={ () => this.setState({ showPressed: !this.state.showPressed }) }
          onPressOut={ () => {
            this.props.navigation.navigate('menu', {
              'prevPage': 'home'
            });
            setTimeout(()=>{this.setState({ showPressed: !this.state.showPressed })},1000);
          }}>
          {this.renderButton()}
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainWrap: {
    flex: 1
  },
  mainBackground: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuBackground: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuName: {
    position: 'absolute',
    width: '40%',
    height: '22%',
    top: '-10%',
  },
  menuContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignItems: 'center',     
    maxWidth: '85%',
  },
  textMenuItem: {
    fontFamily: 'PeaceSans',
    color: 'white',
    textShadowColor: 'rgba(0, 0, 0, 1)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },
  closeBtnBackground: {
    position: 'absolute',
    width: '15%',
    height: '15%',
    top: '-5%',
    right: '-5%',
    zIndex: 20,
  },
  closeButton: {
    position: 'absolute',
    zIndex: 99,
    width: '100%',
    height: '100%',
  },
  btnMenuImage: {
    position: 'absolute',
    bottom: '4%',
    right: 0,
    width: '15%',
    height: '15%',
  },
  btnMenu: {
    width: '100%',
    height: '100%',
  },
});

function mapStateToProps(state) {
  return {
    dataCalendar: state.menu.dataCalendar,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...MenuActions, ...HomeActions}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Calendar)