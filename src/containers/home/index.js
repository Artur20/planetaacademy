import React from 'react';
import { Animated, Easing, Text, View, TextInput, Dimensions, ImageBackground, StyleSheet, TouchableOpacity, Image, NetInfo, Platform } from 'react-native';
import Orientation from 'react-native-orientation';
import RF from "react-native-responsive-fontsize"
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as HomeActions from '../../actions/home'
import LottieView from 'lottie-react-native';
import LoopAnimation from '../../components/loopAnimation';
import SplashScreen from 'react-native-splash-screen';
import AudioHome from '../../components/audioComponents/audioHome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const unpressed = require('../../assets/img/btn_menu-yellow.png');
const pressed = require('../../assets/img/btn_menu_pressed.png');
const mainLogo = require('../../assets/img/main_logo.png');

class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      vw: (Platform.OS == 'ios' && Orientation.getInitialOrientation() === 'PORTRAIT') ? 2 :(Platform.OS == 'ios' && Orientation.getInitialOrientation() === 'LANDSCAPE') ? 2.5 : 3.5,
      name: props.name,
      showPressed: true,
      showCleanBtn: false,
      progress: new Animated.Value(0),
      fadeAnim: new Animated.Value(0.4),
      activeHomeaudio: false,
    }
  }
  
  componentDidMount() {
    Orientation.lockToLandscape();
    SplashScreen.hide();
    
    if (!this.props.animationFinished){
      Animated.timing(this.state.progress, {
        toValue: 1,
        duration: 5000,
        easing: Easing.linear,
        finished: true,
      }).start( () => {
        this.props.setAnimationDone();
        this.animation();     
      });
    } 
    if (this.props.animationFinished) {
      this.animation();
    } 
    this.props.getName(); 
    this.props.changePage('home');
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.name !== this.state.name) {
      this.setState({name: nextProps.name});
    }
  }

  _saveName = () => {
    transliterate = (
      function() {
        let
          rus = "щ   ш  ч  ц  ю  я  ё  ж  ъ  ы  э  а б в г д е з и й к л м н о п р с т у ф х ь".split(/ +/g),
          eng = "shh sh ch cz yu ya yo zh `` y' e` a b v g d e z i j k l m n o p r s t u f x `".split(/ +/g)
        ;
        return function(text, engToRus) {
          let x;
          for(x = 0; x < rus.length; x++) {
            text = text.split(engToRus ? eng[x] : rus[x]).join(engToRus ? rus[x] : eng[x]);
            text = text.split(engToRus ? eng[x].toUpperCase() : rus[x].toUpperCase()).join(engToRus ? rus[x].toUpperCase() : eng[x].toUpperCase());	
          }
          return text;
        }
      }
    )();
    this.props.findName(this.state.name, transliterate(transliterate(this.state.name), true));
    if (this.state.name.length) {
      this.setState({ activeHomeaudio: true})
    }
  }

  _showHelloAudio = () => {
    this.setState({ activeHomeaudio: false})
  }

  animation = () => {
    Animated.timing(
      this.state.fadeAnim,
      {
        toValue: 1,
        duration: 1000,
      }
    ).start();
  }

  renderButton() {
    return (
      <Image
        style={ styles.btnMenu }
        resizeMode='contain'
        source={ this.state.showPressed ? unpressed : pressed }
      />
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    const nextStepBtn = 
      <ImageBackground 
        style={ styles.btnNameBg } 
        resizeMode='contain' 
        source={require('../../assets/img/btn_green.png')}>
          <TouchableOpacity
            style={styles.btnName}
            onPress={()=>{
              navigate('cleaneTeeth');
            }}>
          <Text style={[styles.btnTextName, { fontSize: RF(this.state.vw) } ]}>Почистить зубы</Text>
        </TouchableOpacity>
      </ImageBackground>;
    const nameForm =
      <View style={ styles.formWrap }>
        <ImageBackground 
          style={ styles.btnNameBg } 
          resizeMode='contain' 
          source={require('../../assets/img/btn_yellow.png')}>
          <TextInput
            style={[ styles.textInput, { fontSize: RF(this.state.vw) }]}
            underlineColorAndroid="transparent"
            disableFullscreenUI={true}
            placeholder={"Введите имя"}
            placeholderTextColor={"white"}
            value={this.state.name}
            onFocus={() => this._showHelloAudio()}
            onChangeText={(name) => this.setState({name})}
            onBlur={() => this._saveName()}
          />  
        </ImageBackground>
        {
          nextStepBtn
        }
      </View>;
    const preloader =   
      <LottieView 
        loop={true} 
        source={require('../../assets/scene1.json')} 
        style={{ backgroundColor: '#66b494'}} 
        resizeMode="cover"
        progress={this.state.progress} 
        imageAssetsFolder='animation/images/scene1'
      />;
    const homePage = 
      <KeyboardAwareScrollView keyboardShouldPersistTaps='handled' alwaysBounceVertical={false} style={{backgroundColor: '#04b594'}} contentContainerStyle={{flexGrow: 1}}>
        <Animated.View
          style={{
            position: 'absolute',
            zIndex: 2,
            width: '100%', 
            height: '100%',
            opacity: this.state.fadeAnim,
          }}>
          <LoopAnimation source={require('../../assets/img/clouds.png')} duration={100000} width={Dimensions.get('window').width} height={Dimensions.get('window').height}/>
          <AudioHome name={this.props.keyName} active={this.state.activeHomeaudio}/>
          <View style={ styles.contentWrap }>
            <View style={ styles.leftWrap }>
              <Image
                style={ styles.mainName }
                resizeMode='contain' 
                source={mainLogo} 
              />
              {/* <ImageBackground 
                style={ styles.backImage } 
                resizeMode='contain' 
                source={require('../../assets/img/profile_bg.png')}>
                  <Image
                  style={ styles.profileImage }
                  resizeMode='contain' 
                  source={require('../../assets/img/profile.png')} />
              </ImageBackground> */}
              {/* <ImageBackground 
                style={ styles.btnPhotoImage } 
                resizeMode='contain' 
                source={require('../../assets/img/btn_photo.png')}>
                  <TouchableOpacity
                    style={styles.btnPhoto}
                    onPress={()=>{
                      navigate('menu');
                    }}>
                </TouchableOpacity>
              </ImageBackground> */}
              { nameForm }
            </View>
            <View style={ styles.rightWrap }>
             
              <Image
                style={ styles.lionImage }
                resizeMode='contain' 
                source={require('../../assets/img/lion.png')} 
              />
            </View>
            <TouchableOpacity
              activeOpacity={1}
              style={styles.btnMenuImage}
              onPressIn={ () => this.setState({ showPressed: !this.state.showPressed, activeHomeaudio: false }) }              
              onPressOut={ ()=>{
                navigate('menu', {
                  'prevPage': 'home'
                });
                this.setState({ showPressed: !this.state.showPressed });
                this.props.clearCounter()
              }}>
              { this.renderButton() }
            </TouchableOpacity>
          </View>
        </Animated.View>
      </KeyboardAwareScrollView>
    return (
      (this.props.animationFinished) ? (homePage) : (preloader)
    );
  }
}

const styles = StyleSheet.create({
  mainWrap: {
    flex: 1,
    backgroundColor: '#66b494'
  },
  mainBackground: {
    width: '100%',
    height: '100%',
    paddingHorizontal: '5%',
    paddingTop: '2%',
    paddingBottom: '2%'
  },
  contentWrap: {
    flexDirection: 'row',
    width: '100%',
    height: '100%',
    position: 'absolute',
    left: 0,
    top: 0
  },
  leftWrap: {
    width: '40%',
    height: '100%',
    justifyContent: 'space-between',
    paddingRight: '5%',
  },
  rightWrap: {
    width: '60%'
  },
  btnMenuImage: {
    position: 'absolute',
    bottom: '4%',
    right: '0%',
    width: '15%',
    height: '15%',
  },
  btnMenu: {
    width: '100%',
    height: '100%',
  },
  mainName: {
    width: '100%',
    height: '40%',
    top: '5%',
    left: '15%',
    justifyContent: 'flex-start'
  },
  lionImage: {
    position: 'absolute',
    bottom: '-25%',
    right: 20,
    width: '90%',
    height: '90%'
  },
  formWrap: {
    height: '40%', 
    justifyContent: 'flex-end',
    marginBottom: '10%'
  },
  btnNameBg: {
    width: '100%',
    height: '40%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    marginBottom: '2%'
  },
  btnName: {
    width:'100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute'
  },
  btnTextName: {
    fontFamily: 'PeaceSans',
    color: 'white',
    textShadowColor: 'rgba(0, 0, 0, 1)',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 5,
    textAlign: 'center'
  },
  btnPhotoImage: {
    width: '100%',
    height: '35%',
    marginBottom: 20
  },
  btnPhoto: {
    width: '100%',
    height: '100%'
  },
  textInput: {
    width: '85%',
    height: '100%',
    overflow: 'hidden',
    fontFamily: 'PeaceSans',
    color: 'white',
    textShadowColor: 'rgba(0, 0, 0, 1)',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 5,
    textAlign: 'center',
  },
  backImage: {
    width: '100%',
    height: '35%',
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  profileImage: {
    width: '95%',
    height: '95%',
    borderRadius: 500
  }
})

function mapStateToProps(state) {
  return {
    name: state.home.userName,
    animationFinished: state.home.animationFinished,
    keyName: state.home.keyName,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(HomeActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)