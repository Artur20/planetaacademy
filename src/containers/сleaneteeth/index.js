import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  BackHandler,
  Dimensions,
  Animated
} from 'react-native';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Video from 'react-native-video';
import Sound from 'react-native-sound';
import KeepAwake from 'react-native-keep-awake';
import * as MenuActions from '../../actions/menu'
import * as HomeActions from '../../actions/home'
import ItemCalendar from '../../components/calendar/itemCard'
import RrogressBar from '../../components/progressBar'
import Pause from '../../components/pause'
import AudioCleanTeeth1 from '../../components/audioComponents/audioCleanTeeth1'
import AudioCleanTeeth2 from '../../components/audioComponents/audioCleanTeeth2'

const unpressed = require('../../assets/img/btn_menu-yellow.png');
const pressed = require('../../assets/img/btn_menu_pressed.png');
const unpressedClose = require('../../assets/img/close-btn.png');
const pressedClose = require('../../assets/img/btn_close_pressed.png');
const video2 = require('../../assets/videos/scene_02.mp4');
const video3 = require('../../assets/videos/scene_03.mp4');

class Calendar extends React.Component {
  state = {
    bRatio: 1030 / 558,
    bWidth: null,
    bHeight: null,
    listenAudio: true,
    showPressed: true,
    showPressedClose: true,
    activeFirstVideo: true,
    activeSecondVideo: false,
    duration: 0,
    progress: 0,
    pausedFirstVideo: false,
    pausedSecondVideo: false,
    fadeAnim: new Animated.Value(0),
    AudioCleanTeeth1active: false,
    AudioCleanTeeth2active: false,
  }

  componentDidMount() {
    if (Dimensions.get('window').height <= 558) {
      this.setState({
        bWidth: ((1030 * Dimensions.get('window').height) / 558) * 0.7,
        bHeight: Dimensions.get('window').height * 0.7,
      });
    } else {
      this.setState({
        bWidth: ((1030 * (Dimensions.get('window').width / this.state.bRatio)) / 558) * 0.7,
        bHeight: (Dimensions.get('window').width / this.state.bRatio) * 0.7,
      });
    }

    this.animation();
    KeepAwake.activate();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.props.changePage('cleaneTeeth');
  }

  handleBackButtonClick = () => {
    this._back();
    return true;
  }

  componentWillUnmount() {
    KeepAwake.deactivate();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  _back = () => {
    this.props.navigation.goBack();
  }

  animation = () => {
    Animated.timing(
      this.state.fadeAnim,
      {
        toValue: 1,
        duration: 2000,
      }
    ).start();
  }

  playOrPauseVideo(paused) { 
    if(this.state.activeFirstVideo) {
      this.setState({pausedFirstVideo: !paused});
      this.setState({AudioCleanTeeth1active: false})
    }
    if(this.state.activeSecondVideo) {
      this.setState({pausedSecondVideo: !paused});
    }
  } 

  resetVideo(paused) { 
    if(this.state.activeFirstVideo) {
      this.player.seek(0);
      this.setState({pausedFirstVideo: !paused});
    }
    if(this.state.activeSecondVideo) {
      this.player2.seek(0);
      this.setState({pausedSecondVideo: !paused}); 
    }
  } 
 
  renderPause() { 
    let {pausedFirstVideo, pausedSecondVideo} = this.state;
    let paused = false; 

    if(this.state.activeFirstVideo) {
      paused = pausedFirstVideo;
    }
    if(this.state.activeSecondVideo) {
      paused = pausedSecondVideo;
    }
    
    return ( 
      <View style={ styles.pauseWrap }> 
        <Pause {...this.props} onChange={this.playOrPauseVideo.bind(this, paused)} onReset={this.resetVideo.bind(this, true)}/> 
      </View> 
    ) 
  }

  onProgressFirstVideo = (data) => {
    if (Math.ceil(data.currentTime) === 6 && this.state.listenAudio) {
      console.log('onProgressFirstVideo')
      this.setState({listenAudio: false});
      this.setState({AudioCleanTeeth1active: true})
    }

    if (Math.ceil(data.currentTime) === Math.ceil(data.seekableDuration - 1)) {
      setTimeout(() => this.setState({ activeFirstVideo: false, activeSecondVideo: true , AudioCleanTeeth1active: false}), 500);      
    }
  };

  onProgressSecondVideo = (data) => {
    this.setState({ progress: data.currentTime.toFixed(0) });
    this.setState({ duration: data.seekableDuration });

    if (Math.ceil(data.currentTime) === Math.ceil(data.seekableDuration - 1)) {
      this.props.syncDataCalendar();
      setTimeout(() => {
        this.setState({ activeFirstVideo: false, activeSecondVideo: false });
        //run calendar            
        // this.ovationAudio();
        this.setState({AudioCleanTeeth2active: true})
        this.props.setActionToCalendar();
        this.setState({AudioCleanTeeth2active: false})
      }, 500);    
    }
  };

  renderButton() {
    return (
      <Image
        style={ styles.btnMenu }
        resizeMode='contain' 
        source={ this.state.showPressed ? unpressed : pressed }
      />
    );
  }

  renderButtonClose() {
    return (
      <Image
        style={ styles.closeButton }
        resizeMode='contain' 
        source={ this.state.showPressedClose ? unpressedClose : pressedClose }
      />
    );
  }

  renderScene2() {
    let {pausedFirstVideo} = this.state;

    return (
      <Animated.View
        style={{
          backgroundColor: 'transparent',
          position: 'absolute',
          zIndex: 2,
          width: '100%', 
          height: '100%',
          opacity: this.state.fadeAnim,
        }}
      >
        <TouchableOpacity style={styles.videoView} 
          onPress={this.playOrPauseVideo.bind(this, pausedFirstVideo)}> 
          <Video source={ video2 }
            ref={(ref) => {
              this.player = ref
            }}
            paused={pausedFirstVideo} 
            resizeMode = 'cover'
            onProgress = {this.onProgressFirstVideo}
            style = {styles.backgroundVideo} 
          />
          { 
            (pausedFirstVideo) ? this.renderPause(pausedFirstVideo) : false 
          } 
        </TouchableOpacity> 
      </Animated.View>
    )
  }

  renderScene3() {
    let {pausedSecondVideo} = this.state;

    return (
      <View style={{flex: 1}}>
        <TouchableOpacity style={styles.videoView} 
          onPress={this.playOrPauseVideo.bind(this, pausedSecondVideo)}> 
          <RrogressBar time={this.state.progress} duration={this.state.duration}/>
          <Video source={ video3 }
            ref={(ref) => {
              this.player2 = ref
            }}
            paused={pausedSecondVideo} 
            volume={0.4}
            onLoad={() => {
              this.setState({
                pause: true
              });
            }}
            resizeMode = 'cover'
            onProgress = {this.onProgressSecondVideo}
            style = {styles.backgroundVideo} 
          />
          { 
            (pausedSecondVideo) ? this.renderPause(pausedSecondVideo) : false 
          } 
        </TouchableOpacity> 
      </View>
    )
  }

  renderCalendar() {
    return (
      <ImageBackground 
        style={ styles.mainBackground } 
        resizeMode='cover' 
        source={require('../../assets/img/main-bg.png')}>
        <ImageBackground 
          style={[styles.menuBackground, { width: this.state.bWidth, height: this.state.bHeight }]} 
          source={require('../../assets/img/menu-bg.png')}>
          <Image
            style={ styles.menuName }
            resizeMode='contain' 
            source={require('../../assets/img/calendar_name.png')} 
          />
          <TouchableOpacity
            activeOpacity={1}
            style={styles.closeBtnBackground}
            onPressIn={ () => this.setState({ showPressedClose: !this.state.showPressedClose }) }
            onPressOut={()=> {
              this._back();
            }}>
            {this.renderButtonClose()}
          </TouchableOpacity>
          <View style = {styles.menuContainer}>
            {
              (this.props.dataCalendar) ? (
                this.props.dataCalendar.map((item, index) => (
                  <ItemCalendar key={index} label={item.name} status={item.status} />
                ))
              ) : false
            }  
          </View>
        </ImageBackground>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.btnMenuImage}
          onPressIn={ () => this.setState({ showPressed: !this.state.showPressed }) }
          onPressOut={ () => {
            this.props.navigation.navigate('menu', {
              'prevPage': 'home'
            });
            setTimeout(()=>{this.setState({ showPressed: !this.state.showPressed })},1000);
          }}>
          {this.renderButton()}
        </TouchableOpacity>
      </ImageBackground>
    )
  }
  render() {
    return (
      <View style={ styles.mainWrap }> 
        {
          (this.state.activeFirstVideo) ? this.renderScene2() : (this.state.activeSecondVideo) ? this.renderScene3() : this.renderCalendar()
        }
        <AudioCleanTeeth1 active={this.state.AudioCleanTeeth1active} name={this.props.keyName}/>
        <AudioCleanTeeth2 active={this.state.AudioCleanTeeth2active}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  videoView: { 
    flex: 1, 
    justifyContent: "center", 
    alignItems: "center" 
  },
  pauseWrap: { 
    flex: 1,  
    position: 'absolute',  
    backgroundColor: 'rgba(10, 10, 10, 0.3)',  
    zIndex: 99,  
    justifyContent: 'center',  
    alignItems: 'center',  
    width: '100%', 
    height: '100%' 
  }, 
  mainWrap: {
    flex: 1,
    backgroundColor: '#66b494',
  },
  backgroundVideo: {
    position: 'absolute',    
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 0,
  },
  mainBackground: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuBackground: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuName: {
    position: 'absolute',
    width: '40%',
    height: '22%',
    top: '-10%',
  },
  menuContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignItems: 'center',     
    maxWidth: '85%',
  },
  textMenuItem: {
    fontFamily: 'PeaceSans',
    color: 'white',
    textShadowColor: 'rgba(0, 0, 0, 1)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },
  closeBtnBackground: {
    position: 'absolute',
    width: '15%',
    height: '15%',
    top: '-5%',
    right: '-5%',
    zIndex: 20,
  },
  closeButton: {
    position: 'absolute',
    zIndex: 99,
    width: '100%',
    height: '100%',
  },
  btnMenuImage: {
    position: 'absolute',
    bottom: 20,
    right: 17,
    width: '15%',
    height: '15%',
  },
  btnMenu: {
    width: '100%',
    height: '100%',
  },
});

function mapStateToProps(state) {
  return {
    dataCalendar: state.menu.dataCalendar,
    keyName: state.home.keyName,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...MenuActions, ...HomeActions}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Calendar)