import { StackNavigator } from 'react-navigation';
import { MAIN_COLOR } from '../config'

import HomeScreen from '../containers/home';
import MenuScreen from '../containers/menu';
import AboutScreen from '../containers/about';
import CleaneTeethScreen from '../containers/сleaneteeth';
import CalendarScreen from '../containers/calendar';

const MainStackRouter = StackNavigator({
    home: { 
        screen: HomeScreen,
        navigationOptions: {
            header: null
        }
    },
    menu: { 
        screen: MenuScreen,
        navigationOptions: {
            header: null
        }
    },
    cleaneTeeth: {
        screen: CleaneTeethScreen,
        navigationOptions: {
            header: null
        }
    },
    calendar: {
        screen: CalendarScreen,
        navigationOptions: {
            header: null
        }
    },
    about: {
        screen: AboutScreen,
        navigationOptions: {
            title: 'О центре Planeta Academy',
            headerTintColor: '#ffffff',
            headerStyle: {
                backgroundColor: MAIN_COLOR,  
            },
            headerTitleStyle: {
                width: '100%',
                marginHorizontal: 0
            }
        }
    },
});

export default MainStackRouter;