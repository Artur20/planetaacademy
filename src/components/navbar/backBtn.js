import React, { Component } from "react";
import { Button, Icon } from 'native-base';
import { connect } from 'react-redux';

import * as HomeActions from '../../actions/home';
import { bindActionCreators } from 'redux';

import Orientation from 'react-native-orientation';

class BackBtn extends Component {
  pressFunc() {
    this.props.navigation.goBack();
    this.props.changeCounter(-1);
    if (this.props.counter == 0) {
      Orientation.lockToLandscape();
    }
  }

  render() {
    return (
      <Button transparent light onPress={() => this.pressFunc()}>
        <Icon name='arrow-back' />
      </Button>
    )
  }
}
function mapStateToProps(state) {
  return {
    counter: state.home.counterHistoty
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(HomeActions, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(BackBtn)