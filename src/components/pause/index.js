import React from 'react';
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  View
} from 'react-native';

const unpressed = require('../../assets/img/close-btn.png');
const pressed = require('../../assets/img/btn_close_pressed.png');
const unpressedReload = require('../../assets/img/btn_reload.png');
const pressedReload = require('../../assets/img/btn-reload-pressed.png');
const unpressedPlay = require('../../assets/img/btn_play.png');
const pressedPlay = require('../../assets/img/play-btn-pressed.png');

class Pause extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      pRatio: 696 / 278,
      pWidth: null,
      pHeight: null,
      showPressedReload: true,
      showPressedPlay: true,
      showPressed: true,
    }
  }

  componentDidMount() {
    if (Dimensions.get('window').height <= 278) {
      this.setState({
        pWidth: ((696 * Dimensions.get('window').height) / 278) * 0.5,
        pHeight: Dimensions.get('window').height * 0.5,
      });
    } else {
      this.setState({
        pWidth: ((696 * (Dimensions.get('window').width / this.state.pRatio)) / 278) * 0.5,
        pHeight: (Dimensions.get('window').width / this.state.pRatio) * 0.5,
      });
    }
  }

  renderButtonReload() {
    let imgSource = this.state.showPressedReload? unpressedReload : pressedReload;
    return (
      <Image
        style={ styles.btnMenu }
        source={ imgSource }
        resizeMode="contain"
      />
    );
  }

  renderButtonPlay() {
    let imgSource = this.state.showPressedPlay? unpressedPlay : pressedPlay;
    return (
      <Image
        style={ styles.btnMenu }
        source={ imgSource }
        resizeMode="contain"
      />
    );
  }

  renderButton() {
    return (
      <Image
        style={ styles.btnMenu }
        source={ this.state.showPressed ? unpressed : pressed }
        resizeMode="contain"
      />
    );
  }

  render() {
    return (
      <ImageBackground
        style={[ styles.pauseBackground, { width: this.state.pWidth, height: this.state.pHeight }]}
        source={require('../../assets/img/pause-bg.png')}>
        <Image
          style={ styles.pauseName }
          resizeMode='contain' 
          source={require('../../assets/img/pause-name.png')} 
        />
        <View style={ styles.pauseButtonsWrap }>
          <TouchableOpacity
            activeOpacity={1}
            style={ styles.btnPauseImage }
            onPressIn={() => this.setState({ showPressedPlay: !this.state.showPressedPlay }) }
            onPressOut={() => {
              this.props.onChange(false);
              this.setState({ showPressedPlay: !this.state.showPressedPlay });
            }}>
            {this.renderButtonPlay()}
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={1}
            style={ styles.btnPauseImage }
            onPressIn={ () => this.setState({ showPressedReload: !this.state.showPressedReload }) }
            onPressOut={ () => {
              this.props.onReset(true);
              this.setState({ showPressedReload: !this.state.showPressedReload });
            }}>
            {this.renderButtonReload()}
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={1}
            style={styles.btnPauseImage}
            onPressIn={ () => this.setState({ showPressed: !this.state.showPressed }) }
            onPressOut={ () => {
              this.props.navigation.navigate('home');
              this.setState({ showPressed: !this.state.showPressed });
            }}>
            {this.renderButton()}
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  pauseBackground: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
  },
  pauseName: {
    position: 'absolute',
    width: '60%',
    height: '35%',
    top: '-10%',
  },
  pauseButtonsWrap: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center', 
    width: '80%',
  },   
  btnPauseImage: {
    width: '60%',
    height: '60%',
    marginHorizontal: '5%'
  },
  btnMenu: {
    width: '100%',
    height: '100%',
  },
});

export default Pause;