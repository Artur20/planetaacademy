import React, { Component } from 'react'; 
import { 
  StyleSheet, 
  View, 
  Animated, 
  Dimensions,
  Easing,
} from 'react-native';
import {MAIN_COLOR} from '../../config';

const styles = StyleSheet.create({ 
	main: { 
		flex: 1, 
		flexDirection: 'row', 
		position: 'absolute',
		backgroundColor: MAIN_COLOR, 
	}
}); 

export default class LoopAnimation extends Component { 
	constructor(props){ 
		super(props); 

		this.state={ 
			move_1: new Animated.Value(0),
			move_2: new Animated.Value(0),
			width: 0, 
		}; 
	}
	

	runAnimation() {      
		Animated.sequence([
			Animated.timing(this.state.move_2, {       
				toValue: -2*this.state.width,  
				duration: 0,  
			}),
			Animated.parallel([  
				Animated.timing(this.state.move_2, {       
					toValue: -this.state.width,  
					duration: this.props.duration,  
					easing: Easing.linear,  
				}),
				Animated.timing(this.state.move_1, {  
					toValue: this.state.width,    
					duration: this.props.duration,  
					easing: Easing.linear,  
				}), 
			]),                           
			Animated.timing(this.state.move_1, {       
				toValue: -this.state.width,  
				duration: 0,  
			}),      
			Animated.parallel([       
				Animated.timing(this.state.move_2, {  
					toValue: 0,    
					duration: this.props.duration,  
					easing: Easing.linear,  
				}), 
				Animated.timing(this.state.move_1, {  
					toValue: 0,    
					duration: this.props.duration,  
					easing: Easing.linear,  
				}),
			]),
		]).start(() => {  
			this.runAnimation();  
		});  
	}

	getWidth() { 
		this.setState({ width: this.props.width }); 
	} 

	render() { 
		return( 
			<View style={styles.main}> 
				<Animated.Image 
					onLayout = { this.getWidth.bind(this) } 
					onLoadEnd = { () => { this.runAnimation() } }
					source = { this.props.source } 
					style = {{ width: this.props.width, height: this.props.height, transform: [{ translateX: this.state.move_1 }] }}
					resizeMode="contain"
				/> 
				<Animated.Image 
					source = { this.props.source } 
					style = {{ width: this.props.width, height: this.props.height, transform: [{ translateX: this.state.move_2 }] }}
					resizeMode="contain"
				/> 
			</View> 
		); 
	}; 

}; 