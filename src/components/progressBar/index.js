import React from 'react';
import {StyleSheet, View, ImageBackground} from 'react-native';
import {Text} from 'native-base';
import moment from 'moment';

import {MAIN_COLOR} from '../../config';

const ProgressBar = ({ time, duration }) => {
	outputTime = (sec) => {
		let totalDuration = duration * 1000;
		let currentTime = time * 1000;

		return moment.utc(totalDuration - currentTime).format('mm:ss');
	}

	outputProgress = (sec, duration) => {
		return 100 - ((100 / duration) * sec); 
	}

	return (
		<View style={ styles.mainWrap }>
			<ImageBackground 
				style={ styles.progressBackground } 
				resizeMode='contain' 
				source={require('../../assets/img/progress-bg.png')}>
				<View style = {{ 
					backgroundColor: MAIN_COLOR,
					minWidth: '3%',
					maxWidth: '91%',
					width: `${this.outputProgress(time, duration)}%`,
					height: '62%',
					position: 'absolute',
					borderRadius: 20,
					left: 20,
					right: 20,
					top: 7
				}}></View>
				<Text style={ styles.progressText }>{this.outputTime(time, duration)}</Text>
			</ImageBackground>
		</View>
	);
}

const styles = StyleSheet.create({
	mainWrap: {
		flex: 1,
		position: 'absolute',
		width: '100%',
		zIndex: 2,
		top: 20,
		justifyContent: 'center',
		alignItems:'center',
	},
	progressBackground: {
		width: 400,
		height: 40,
		justifyContent: 'flex-start',
		alignItems:'flex-start',
		position: 'relative',
	},
	progressText: {
		width: '100%',
		textAlign: 'center',
		color: 'white',
		position: 'absolute',
		top: 8
	}
})

export default ProgressBar;