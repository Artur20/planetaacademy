import React, { Component } from 'react';
import {
  StyleSheet,
  ImageBackground,
  Dimensions
} from 'react-native';
import {Text} from 'native-base';

const btnComplete = require('../../assets/img/green_calendar.png');
const btnProgress = require('../../assets/img/yellow_calendar.png');
const btnFail = require('../../assets/img/red_calendar.png');
const btnFuture = require('../../assets/img/grey_calendar.png');

export default class ItemCalendar extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      size: 2 * (Dimensions.get('window').width / 100)
    }
  }

  render() {
    return (
      <ImageBackground 
        style={ styles.calendarItem } 
        resizeMode='contain' 
        source={(this.props.status === 'complete')? btnComplete: (this.props.status === 'progress')? btnProgress: (this.props.status === 'fail')? btnFail : btnFuture}>
        <Text style={ [ styles.calendarText, { fontSize: this.state.size} ]} >{this.props.label}</Text>
      </ImageBackground>
    )
  };
}

const styles = StyleSheet.create({
  calendarItem: {
    width: '24%',
    height: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: '1%'
  },
  calendarText: {
    textAlign: 'center',
    fontFamily: 'PeaceSans',
    color: 'white',
  },
});