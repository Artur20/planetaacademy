import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  View,
  TouchableOpacity
} from 'react-native';
import {Text} from 'native-base';
import { StackActions, NavigationActions } from 'react-navigation';

const unpressedMenu = require('../../assets/img/btn_yellow.png');
const pressedMenu = require('../../assets/img/btn_yellow_pressed.png');

export default class MenuBtn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPressedMenu: true 
    }
  }

  _navigateToWalkthrough = (root) => {
    const resetAction = StackActions.reset({
      index: 1,
      actions: [
        NavigationActions.navigate({ routeName: 'home'}),
        NavigationActions.navigate({ routeName: root})
      ]
    });

    this.props.navigation.dispatch(resetAction);
  }

  renderButtonMenu() {
    return (
      <Image
        style={ styles.buttonImage }
        resizeMode='contain'
        source={ this.state.showPressedMenu ? unpressedMenu : pressedMenu }
      />
    );
  }

  render() {
    let {is_list_menu} = this.props

    return (
      <View style = { [styles.buttonBackground, (is_list_menu)? {height: '25%'}: {height: '35%'}] }>
        {this.renderButtonMenu()}
        <TouchableOpacity
          activeOpacity={1}
          style={styles.menuButton}
          onPressIn={ () => this.setState({ showPressedMenu: !this.state.showPressedMenu }) }
          onPressOut={()=>{
            this._navigateToWalkthrough(this.props.link);
          }}
        >
          <Text style={{fontFamily: 'PeaceSans', color: 'white',  textShadowColor: 'rgba(0, 0, 0, 1)', textShadowOffset: {width: 1, height: 1}, textShadowRadius: 5, fontSize: this.props.size }} > {this.props.label} </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonBackground: {
    width: '45%',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical:3,
    position: 'relative'
  },
  buttonImage: {
    width:'100%',
    height: '100%'
  },
  menuButton: {
    width:'100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute'
  },
});