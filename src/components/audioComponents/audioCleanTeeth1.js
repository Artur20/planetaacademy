import React, { Component } from 'react';
import {
    View
  } from 'react-native';
import Sound from 'react-native-sound';

class AudioCleanTeeth1 extends Component{
  listenAudio(name) {
    if (name) {
      const trackName = new Sound(`privet${name}.mp3`, Sound.MAIN_BUNDLE, () => {
        trackName.play((success) => {
          if (success) {
            this.listenAudio2();
          }
        });
      });
    } else {
      this.listenAudio2();
    }
  }
    
  listenAudio2() {
    const trackNoName = new Sound(`davay.mp3`, Sound.MAIN_BUNDLE, () => {
      trackNoName.play();
    })
  }
    
  render(){
      return(
        <View>
            {
              (this.props.active)?
              this.listenAudio(this.props.name): null
            }
        </View>
      )  
  }
}export default AudioCleanTeeth1