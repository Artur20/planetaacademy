import React, { Component } from 'react';
import {
    View
  } from 'react-native';
import Sound from 'react-native-sound';

class AudioCleanTeeth2 extends Component{
    
  ovationAudio() {
    const trackOvation = new Sound(`ovation.mp3`, Sound.MAIN_BUNDLE, () => {
      trackOvation.play();
    })
  }

  render(){
      return(
        <View>
            {
              (this.props.active)?
              this.ovationAudio(): null
            }
        </View>
      )  
  }
}export default AudioCleanTeeth2