import React, { Component } from 'react';
import {
    View
  } from 'react-native';
import Sound from 'react-native-sound';

export default class AudioHome extends Component{
  constructor(props) {
    super(props);
  }

  listenAudio(name) {
    const trackName = new Sound(`privet${name}.mp3`, Sound.MAIN_BUNDLE, () => {
			trackName.play()
		});
  }
    
  render(){
    return(
      <View>
          {
            (this.props.active)?
            this.listenAudio(this.props.name): null
          }
      </View>
    )  
  }
}