import * as types from '../types/menu';

const initialState = {
  dataCalendar: []
}
  
export default function menuReducer(state = initialState, action) {
  switch (action.type) {
    case types.SET_DATA_CALENDAR:
      return {
        ...state,
        dataCalendar: action.arrayDays
      }
    default:
      return state
  }
}