import * as types from '../types/home';

const initialState = {
  userName: '',
  keyName: null,
  animationFinished: false,
  prevPage: null,
  currentPage: null,
  counterHistoty: 0,
}
  
export default function homeReducer(state = initialState, action) {
  switch (action.type) {
    case types.SET_NAME:
      return Object.assign({}, state, {
        userName: (action.name) ? action.name.userName: '',
        keyName: (action.name) ? action.name.keyName: null,
      });
    case types.ANIMATION_DONE:
      return Object.assign({}, state, {
        animationFinished: action.animationFinished,
      });
    case types.SET_PAGE:
      return Object.assign({}, state, {
        prevPage: state.currentPage,
        currentPage: action.page 
      });
    case types.CHANGE_COUNTER:
      return Object.assign({}, state, {
        counterHistoty: state.counterHistoty + action.payload
      });
    case types.CLEAR_COUNTER:
      return Object.assign({}, state, {
        counterHistoty: 0
      });
    default:
      return state
  }
}