import { combineReducers } from 'redux';
import menu from './menu';
import about from './about';
import home from './home';

const rootReducer = combineReducers({
    menu,
    about,
    home
})

export default rootReducer
