import * as types from '../types/about';

const initialState = {
  about: {
      images: [],
      text: ''
  },
}

  
export default function aboutReducer(state = initialState, action) {
  switch (action.type) {
    case types.SET_ABOUT_DATA:
      return {
        ...state,
        about: action.about
      }
    default:
      return state
  }
}