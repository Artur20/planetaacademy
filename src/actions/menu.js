import {Alert, AsyncStorage} from 'react-native';
import * as types from '../types/menu';

export function syncDataCalendar() {
  return dispatch => {
    _retrieveData('dataCalendar')
      .then((response) => {
        if (response === null) {
          dispatch(createDataCalendar());
        } else {
          dispatch(mergeDataCalendar(JSON.parse(response)));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

export function setActionToCalendar() {
  const d = new Date();
  const currentDay = (d.getDay() === 0) ? 7 : d.getDay();
  const currentHour = d.getHours();
  return (dispatch, getState) => {
    const { menu } = getState();
    let arrayDays = menu.dataCalendar.slice();
    if (arrayDays[currentDay-1].data) {
      (arrayDays[currentDay-1].data[0].status === 'fail' && currentHour >= 6 && currentHour <= 12 ) ? arrayDays[currentDay-1].data[0].status = 'complete': false;
      (arrayDays[currentDay-1].data[1].status === 'fail' && currentHour >= 18 && currentHour <= 24 ) ? arrayDays[currentDay-1].data[1].status = 'complete': false;
      if (arrayDays[currentDay-1].data[0].status === arrayDays[currentDay-1].data[1].status && arrayDays[currentDay-1].data[1].status === 'fail') {
        arrayDays[currentDay-1].status = (currentHour <= 18)? 'progress':'fail';
      } else if (arrayDays[currentDay-1].data[0].status !== arrayDays[currentDay-1].data[1].status) {
        arrayDays[currentDay-1].status = (arrayDays[currentDay-1].data[0].status === 'complete' && currentHour < 20 )? 'complete' : 'progress';
      } else if (arrayDays[currentDay-1].data[0].status === arrayDays[currentDay-1].data[1].status && arrayDays[currentDay-1].data[1].status === 'complete') {
        arrayDays[currentDay-1].status = 'complete';
      }
    } else {
      let data = [{
        status : (currentHour >= 6 && currentHour <= 12) ? 'complete' : 'fail',
      },{
        status : (currentHour >= 18 && currentHour <= 24) ? 'complete' : 'fail',
      }]
      if (data[0].status === data[1].status && data[1].status === 'fail') {
        arrayDays[currentDay-1].status =  (currentHour <= 18)? 'progress':'fail';
      } else if (data[0].status !== data[1].status) {
        arrayDays[currentDay-1].status = (data[0].status === 'complete' && currentHour < 20 )? 'complete' : 'progress';
      } else if (data[0].status === data[1].status && data[1].status === 'complete') {
        arrayDays[currentDay-1].status = 'complete';
      }
      arrayDays[currentDay-1].data = data;
    }
    dispatch(saveDataCalendar(arrayDays));
  }
}

function mergeDataCalendar(data) {
  return dispatch => {
    const d = new Date();
    const currentDay = (d.getDay() === 0) ? 7 : d.getDay();
    if (currentDay === 1) {
      dispatch(createDataCalendar());
    } else {
      const currentHour = d.getHours();
      let arrayDays = data.slice();
      
      arrayDays.forEach((item, index) => {
        if (item.day < currentDay) {
          if (item.status === 'future') {
            arrayDays[index].status = 'fail' 
          } else if (item.status === 'progress') {
            (item.data[0].status === item.data[1].status && item.data[0].status === 'fail') ? arrayDays[index].status = 'fail' : false;
          }
          (item.data) ? delete item.data: false;
        } else if (item.day === currentDay) {
          if (item.status === 'complete') {
            (item.data[1].status === 'fail' && currentHour > 20) ? arrayDays[index].status = 'progress': false;    
          } else if (item.status === 'future') {
            arrayDays[index].status = 'progress';
          }
        }
      })
      dispatch(saveDataCalendar(arrayDays));
    }
  }
}

function createDataCalendar() {
  return dispatch => {
    const d = new Date();
    const currentDay = (d.getDay() === 0) ? 7 : d.getDay();
    const currentHour = d.getHours();
    let arrayDays = [];
    for (let i = 1; i <= 7; i++ ) {
      if (i < currentDay) {
        arrayDays.push({
          day: i,
          name: getNameDay(i),
          status: 'complete'
        })
      } else if (i === currentDay) {
        arrayDays.push({
          day: i,
          name: getNameDay(i),
          status: 'progress',
        })
      } else {
        arrayDays.push({
          day: i,
          name: getNameDay(i),
          status: 'future'
        })
      }
    }
    dispatch(saveDataCalendar(arrayDays))
  }
}

function getNameDay(day) {
  return (day==1)?'пн':(day == 2)?'вт':(day==3)?'ср':(day==4)?'чт':(day==5)?'пт':(day==6)?'сб':'вс';
}

function saveDataCalendar(arrayDays) {
  _storeData('dataCalendar', arrayDays);
  return {
    type: types.SET_DATA_CALENDAR,
    arrayDays: arrayDays
  }
}

//Storage
_storeData = async (name, params) => {
  try {
    await AsyncStorage.setItem(name, JSON.stringify(params));
  } catch (error) {
    Alert.alert(JSON.stringify(error));
  }
}

_retrieveData = async (name) => {
  try {
    const value = await AsyncStorage.getItem(name);
    return value;
   } catch (error) {
    Alert.alert(JSON.stringify(error))
   }
}