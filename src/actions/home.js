import {Alert, AsyncStorage} from 'react-native';
let ArrayNames = require('../../names.json');
import * as types from '../types/home';

export function setAnimationDone() {
  return {
    type: types.ANIMATION_DONE,
    animationFinished: true,
  }
}

export function getName() {
  return dispatch => {
    _retrieveData('user_name')
    .then((response) => {
      dispatch(nameSuccess(JSON.parse(response)));
    })
    .catch((error) => {
      console.log(error);
    });
  }
}

export function findName(userName_original, userName_translated) {
  let foundName = null;
  let obj = {};
  
  if (userName_translated.length) {
    for (var name in ArrayNames) {
      ArrayNames[name].forEach((item) => {
        if (item.toLowerCase() === userName_translated.toLowerCase()) {
          foundName = name;
        }
      })
    }
  }
  obj.userName = userName_original;
  obj.keyName = foundName;
 
  return dispatch => {
    _storeData('user_name', obj);
    dispatch(nameSuccess(obj));
  }
}

function nameSuccess(data) {
  return {
    type: types.SET_NAME,
    name: data
  }
}

export function changePage(data){
  return dispatch => {
    dispatch(changePageSuccess(data));
  }
}

function changePageSuccess(currentPage){
  return {
    type: types.SET_PAGE,
    page: currentPage
  }
}

export function changeCounter(number){
  return{
    type: types.CHANGE_COUNTER,
    payload: number
  }
}

export function clearCounter(){
  return{
    type: types.CLEAR_COUNTER
  }
}

//Storage
_storeData = async (name, params) => {
  try {
    await AsyncStorage.setItem(name, params);
  } catch (error) {
    Alert.alert(JSON.stringify(error));
  }
}