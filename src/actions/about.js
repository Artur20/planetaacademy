import * as types from '../types/about';
import {ABOUT_TEXT} from '../config'

export function getAboutData() {
  return (dispatch) => {
    dispatch(aboutSuccess({
      text: ABOUT_TEXT
    }));
  }
}

function aboutSuccess(data) {
  return {
    type: types.SET_ABOUT_DATA,
    about: data
  }
}