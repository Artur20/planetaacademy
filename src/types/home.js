export const SET_NAME = 'SET_NAME';
export const ANIMATION_DONE = 'ANIMATION_DONE';
export const SET_PAGE = 'SET_PAGE';
export const CHANGE_COUNTER = 'CHANGE_COUNTER';
export const CLEAR_COUNTER = 'CLEAR_COUNTER'
