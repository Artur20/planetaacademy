let config = require('../../config.json');

export const MAIN_COLOR = config.main_color;
export const ABOUT_TEXT = config.about_text;
export const API_URL = config.api_url;
